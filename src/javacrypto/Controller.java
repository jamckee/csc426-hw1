package javacrypto;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

public class Controller {

    public TextArea tb_orig, tb_new, tb_find;
    public ChoiceBox<String> cb_key_size, cb_ic, cb_cipher;
    public TextField tb_00, tb_01, tb_02, tb_03, tb_04, tb_05, tb_06, tb_07, tb_08, tb_09, tb_10, tb_11;
    public Button btn_00_up, btn_01_up, btn_02_up, btn_03_up, btn_04_up, btn_05_up, btn_06_up, btn_07_up,
                  btn_08_up, btn_09_up, btn_10_up, btn_11_up;
    public Button btn_00_down, btn_01_down, btn_02_down, btn_03_down, btn_04_down, btn_05_down, btn_06_down, btn_07_down,
                  btn_08_down, btn_09_down, btn_10_down, btn_11_down;
    public Label lbl_key, lbl_ic, lbl_ic_selected, lbl_three, lbl_four, lbl_five, lbl_six,
                 lbl_comm_3, lbl_comm_4, lbl_comm_5, lbl_comm_6,
                 lbl_freq_amounts;
    public Label lbl_a, lbl_b, lbl_c, lbl_d, lbl_e, lbl_f, lbl_g, lbl_h, lbl_i, lbl_j, lbl_k, lbl_l, lbl_m,
                 lbl_n, lbl_o, lbl_p, lbl_q, lbl_r, lbl_s, lbl_t, lbl_u, lbl_v, lbl_w, lbl_x, lbl_y, lbl_z,
                 lbl_alphabet;
    private int key_size = 0;
    private String key = "";
    private String text;
    private String dText;
    private String broken;
    private boolean vigenere = true;
    private List<Double> eng_freq_list = Arrays.asList(0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228, 0.02015,
            0.06094, 0.06966, 0.00153, 0.00772, 0.04025, 0.02406, 0.06749,
            0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758,
            0.00978, 0.02360, 0.00150, 0.01974, 0.00074);
    private Map<String,TextField> alphas = new HashMap<>();
    private Map<String,Label> alpha_freq = new HashMap<>();
    private Double[] ic_array = new Double[12];
    private boolean spaces = true;
    private boolean brute = false;

    @FXML
    public void initialize(){
        //set map for breakout boxes
        alphas.put("00",tb_00);alphas.put("01",tb_01);alphas.put("02",tb_02);alphas.put("03",tb_03);alphas.put("04",tb_04);alphas.put("05",tb_05);
        alphas.put("06",tb_06);alphas.put("07",tb_07);alphas.put("08",tb_08);alphas.put("09",tb_09);alphas.put("10",tb_10);alphas.put("11",tb_11);
        //set text for common words
        lbl_comm_3.setText("Common 3:\nthe\nand\nfor\nyou\nnot\nare\nall\nnew\nwas\ncan\nhas");
        lbl_comm_4.setText("Common 4:\nthat\nthis\nwith\nfrom\nyour\nhave\nmore\nwill\nhome\npage\nfree");
        lbl_comm_5.setText("Common 5:\nabout\nother\nwhich\ntheir\nthere\nfirst\nwould\nthese\nclick\nprice\nstate");
        lbl_comm_6.setText("Common 6:\nsearch\nonline\npeople\nhealth\nshould\nsystem\npolicy\nnumber\nplease\nrights\npublic");
        //set map for frequency letter boxes
        alpha_freq.put("0",lbl_a);alpha_freq.put("1",lbl_b);alpha_freq.put("2",lbl_c);alpha_freq.put("3",lbl_d);
        alpha_freq.put("4",lbl_e);alpha_freq.put("5",lbl_f);alpha_freq.put("6",lbl_g);alpha_freq.put("7",lbl_h);
        alpha_freq.put("8",lbl_i);alpha_freq.put("9",lbl_j);alpha_freq.put("10",lbl_k);alpha_freq.put("11",lbl_l);
        alpha_freq.put("12",lbl_m);alpha_freq.put("13",lbl_n);alpha_freq.put("14",lbl_o);alpha_freq.put("15",lbl_p);
        alpha_freq.put("16",lbl_q);alpha_freq.put("17",lbl_r);alpha_freq.put("18",lbl_s);alpha_freq.put("19",lbl_t);
        alpha_freq.put("20",lbl_u);alpha_freq.put("21",lbl_v);alpha_freq.put("22",lbl_w);alpha_freq.put("23",lbl_x);
        alpha_freq.put("24",lbl_y);alpha_freq.put("25",lbl_z);
        tb_orig.focusedProperty().addListener((ov, oldV, newV) -> {
            if (!newV) { // focus lost
                text = tb_orig.getText().toUpperCase();
            }
        });

    }

    //update selected key size then decrypt again
    public void checkSize(){
        key_size = Integer.parseInt(cb_key_size.getValue());
        updateKey();
        decrypt();
    }

    //update key from breakout textboxes
    private void updateKey(){
        int index = 0;
        TextField tf;
        if (key_size <= 0){
            return;
        }

        StringBuilder temp = new StringBuilder(key_size);
        char tempLetter;
        int tempOffset;

        while (index < key_size) {
            tf = alphas.get((index < 10) ? "0" + index : "" + index);
            if (tf.getText().compareTo("") == 0){
                temp.append("_");
            }else {
                if (!vigenere){
                    tempLetter = tf.getText().toUpperCase().charAt(0);

                    tempOffset = (90 - tempLetter);
                    tempOffset = 65 + tempOffset;
                    if (tempOffset > 90){
                        tempOffset = 65 + (tempOffset - 90);
                    }
                    temp.append((char) (tempOffset));
                }else {
                    temp.append(tf.getText().toUpperCase());
                }
            }
            index++;
        }
        lbl_key.setText(temp.toString().toLowerCase());
        key = temp.toString();
    }

    //change button
    public void changeAlphabet(ActionEvent e){
        Button btn = (Button) e.getSource();
        TextField tf;
        String id = btn.getId();
        String letter;
        char tempLetter = 'a';
        int temp;

        id = id.replaceFirst("btn_","");

        if (id.contains("up")){
            id = id.replaceFirst("_up","");
            tf = alphas.get(id);
            letter = tf.getText();

            if (letter.compareTo("") == 0) letter = "a";
            else if (letter.compareTo("z") == 0)
                letter = "";
            else{
                tempLetter = letter.charAt(0);
                tempLetter++;
                letter = String.valueOf(tempLetter);
            }
        }else if (id.contains("down")){
            id = id.replaceFirst("_down","");
            tf = alphas.get(id);

            letter = tf.getText();

            if (letter.compareTo("") == 0)
                letter = "z";
            else if (letter.compareTo("a") == 0)
                letter = "";
            else{
                tempLetter = letter.charAt(0);
                tempLetter--;
                letter = String.valueOf(tempLetter);
            }
        }else
            return;

        tf.setText(letter);
        updateKey();
        decrypt();
        updateFreqchart(Integer.parseInt(id));
    }

    private void decrypt(){
        StringBuilder original = new StringBuilder(text.length());
        if ((text.length() <= 0) || (key_size <= 0 )|| (key.compareTo("")==0) || key.length() != key_size){
            return;
        }

        dText = "";
        StringBuilder broken = new StringBuilder(text.length());
        StringBuilder decrypted = new StringBuilder(text.length());
        char letter;
        int counter = 0;
        int cOffset = 0;
        int tIndex;
        int offset;

        //swap letters for beaufort
        if (!vigenere){
            for (int i = 0; i < text.length(); i++){
                letter = text.charAt(i);
                if (letter == 95){
                    letter = '_';
                }else if(letter >= 65 && letter <= 90){
                    offset = (90 - letter);
                    offset = 65 + offset;
                    if (offset > 90){
                        offset = 65 + (offset - 90);
                    }
                    letter = (char) (offset);
                }
                original.append(letter);
            }
        }else{
            original.append(text);
        }

        while (counter < original.length()){
            letter = original.charAt(counter);
            tIndex = (counter - cOffset) % key_size;
            if (key.charAt(tIndex) == 95 && (letter >= 65 && letter <= 90)){
                letter = '_';
                broken.append(letter);
                decrypted.append(letter);
            }else{
                if (letter < 65 || letter > 90) {
                    //Skip bad characters
                    cOffset++;
                    broken.append(letter);//pass through for readability
                }else{
                    offset = (char) (key.charAt(tIndex) - 65);
                    letter = (char) (letter - offset);
                    if (letter < 65) {
                        letter = (char) (91 - (65 - letter));
                    }
                    broken.append(letter);
                    decrypted.append(letter);
                }
            }
            if (spaces && counter + 1 >= key_size && ((counter - cOffset + 1) % key_size == 0)) {
                broken.append(" "); //ad spacing based on keysize
            }
            counter++;
        }
        dText = decrypted.toString();
        if (!brute)
            tb_new.setText(broken.toString());
    }

    public void getIC(){
        double ic = 0.0;
        String id;
        TextField tf;
        String[] freqArray;

        for (int k = 1; k < 13; k++) {
            freqArray = createFreqArray(k);

            ic = calcIC(freqArray, k);
            ic_array[k-1] = ic;
        }
        cb_ic.setValue("11");

        for (int i = 0; i < 12; i++){
            if (ic_array[i] >= ic){
                ic = ic_array[i];
                cb_ic.setValue(String.valueOf(i+1));
                cb_key_size.setValue(String.valueOf(i+1));
            }
        }

        lbl_ic.setText(String.format("%10f",ic));
        lbl_ic_selected.setText(String.format("%10f",ic));

        for (int i = 0; i < Integer.parseInt(cb_ic.getValue()); i++){
            if (i < 10){
                id = "0";
            }else{
                id = "";
            }
            id = id + i;
            tf = alphas.get(id);
            tf.setText("a");
        }
        updateKey();
        decrypt();
    }

    private double calcIC(String[] freqArray, int size){
        double ic = 0.0;
        int[] tempFreq;
        double tempIc;
        for (int i = 0; i < size; i++) {
            tempFreq = freqCount(freqArray[i]);
            tempIc = 0.0;
            for (int j = 0; j < 26; j++) {
                tempIc += ((double) tempFreq[j] / freqArray[i].length()) * ((double) (tempFreq[j] - 1) / (freqArray[i].length() - 1));
            }
            ic += tempIc;
        }
        ic = (ic/size);

        return ic;
    }

    private String[] createFreqArray(int size){
        String[] freqArray = new String[size];
        for (int i = 0; i < size; i++) {
            freqArray[i] = "";
            for (int j = i; j < text.length(); j += size) {
                freqArray[i] += text.charAt(j);
            }
        }

        return freqArray;
    }

    private int[] freqCount(String tArray){
        int[] freq = new int[26];
        char letter;

        for (int i = 0; i < 26; i++){
            letter = (char) (65 + i);
            for (int j = 0; j < tArray.length(); j++){
                if (tArray.charAt(j) == letter){
                    freq[i]++;
                }
            }
        }
        return freq;
    }

    public void changeIC(){
        int sel = Integer.parseInt(cb_ic.getValue());
        lbl_ic_selected.setText(String.format("%10f",ic_array[sel-1]));
    }

    public void icLeft(){
        int curr = Integer.parseInt(cb_ic.getValue());
        if (curr > 1){
            cb_ic.setValue(String.valueOf(curr-1));
            lbl_ic_selected.setText(String.format("%10f",ic_array[curr-2]));
        }
    }
    public void icRight(){
        int curr = Integer.parseInt(cb_ic.getValue());
        if (curr < 12){
            cb_ic.setValue(String.valueOf(curr+1));
            lbl_ic_selected.setText(String.format("%10f",ic_array[curr]));
        }
    }

    private void updateFreqchart(int alphabet){
        Label lbl;

        Character letter;
        StringBuilder freqArray = new StringBuilder();
        int[] newArray;
        int tempNum;
        StringBuilder tempNumber = new StringBuilder();

        for (int j = alphabet; j < dText.length(); j += key_size) {
            letter = dText.charAt(j);
            if (letter > 90 || letter < 65 ){
                j -= (key_size - 1);
            }else{
                freqArray.append(letter);
            }

        }
        //freqArray = freqArray(ic_size);
        newArray = freqCount(freqArray.toString());

        for (int i = 0; i < 26; i++){
            tempNum = newArray[i];
            lbl = alpha_freq.get(String.valueOf(i));
            lbl.setText(String.valueOf(tempNum));
        }

        lbl_alphabet.setText("Alphabet: " + freqArray);

        for (int i = 0; i < 26; i++){
            tempNumber.append(String.format("%1$02d", (int) ((eng_freq_list.get(i)) * freqArray.length())));
            if (i < 25) tempNumber.append("    ");
        }
        lbl_freq_amounts.setText(tempNumber.toString());
    }

    public void findRepeats(){
        //long starttime, endtime, totaltime;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        Map<String, Integer> repeats;
        decrypt();

        for (int i = 3; i < 7; i++){
            //starttime = System.nanoTime();
            repeats = findStrings(i);

            repeats.entrySet().stream()
                    .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                    .limit(20)
                    .forEach((k1) -> ps.println(k1.toString().replace("=", ": ")));
            if (i == 3)
                lbl_three.setText(baos.toString());
            else if(i == 4)
                lbl_four.setText(baos.toString());
            else if(i == 5)
                lbl_five.setText(baos.toString());
            else
                lbl_six.setText(baos.toString());

            ps.flush();
            baos.reset();

            //endtime = System.nanoTime();
            //totaltime = endtime - starttime;
            //System.out.println("Find " + i + ": " + (totaltime / 1000000.0));

        }
        ps.close();
        baos.reset();
    }

    private Map<String, Integer> findStrings(int size){
        String search;
        int count;
        Map<String, Integer> repeats = new HashMap<>();


        for(int i = 0; i+size < dText.length(); i++) {
            search = dText.substring(i,i+size);
            count = 0;
            for(int j = i+size; j+size < dText.length(); j++){
                if (dText.substring(j,j+size).compareTo(search) == 0){
                    count++;
                }
            }
            if (count > 0){
                if (!repeats.containsKey(search)){
                    repeats.put(search, count + 1);
                }
            }
        }
        return repeats;
    }

    public void findFirst(){
        String search = tb_find.getText().toUpperCase();
        Boolean space = spaces;
        int index;

        spaces = space;

        if (search.compareTo("") == 0){
            return;
        }

        index = dText.indexOf(search);
        index += (index / key_size);

        tb_new.selectRange(index, index + search.length());

    }

    public void tryAuto(){
        //Step through each letter trying to find closest frequency analysis match
        if (key_size == 0){
            getIC();
        }
        double[] expectedArray = new double[26];
        int[] actualArray;
        String maxKey = "";
        TextField tf;
        Double ic, maxIc = 1e100, tempIc, tempSum;
        StringBuilder freqArray = new StringBuilder(text.length()/key_size);
        char tempLetter;

        //Calculate expected distribution for the entire key: Should only need to calculate once based on substring size
        for (int i = 0; i < 26; i++){
            expectedArray[i] = (eng_freq_list.get(i) * (double) (text.length()/key_size));
        }

        for (int i = 0; i < key_size; i++){
            System.out.println("Checking key at index: " + i);

            for (int j = 0; j < 26; j++){
                tf = alphas.get((i < 10) ? "0" + i : "" + i);

                tempLetter = (char) (65 + j);
                tf.setText(String.valueOf(tempLetter));
                updateKey();
                decrypt();

                //Create letter array
                freqArray.setLength(0);
                for (int k = i; k < dText.length(); k += key_size) {
                    tempLetter = dText.charAt(k);
                    if (tempLetter > 90 || tempLetter < 65 ){
                        k -= (key_size - 1);
                    }else{
                        freqArray.append(tempLetter);
                    }

                }

                //Calculate actual Distribution
                actualArray = freqCount(freqArray.toString());


                tempIc = 0.0;
                for (int k = 0; k < 26; k++) {
                    tempSum = actualArray[k] - expectedArray[k];
                    tempIc += tempSum * tempSum / expectedArray[k];
                }
                ic = tempIc;

                if (ic < maxIc){
                    maxIc = ic;
                    maxKey = key;
                }

            }
            key = maxKey;
            for (int k = 0; k < key.length(); k++){
                tf = alphas.get((k < 10) ? "0" + k : "" + k);
                tempLetter = key.toLowerCase().charAt(k);
                tf.setText(Character.toString(tempLetter));
            }
            updateKey();
            maxIc = 1e100;
        }
        decrypt();
    }
    public void updateMode(){
        vigenere = cb_cipher.getValue().compareTo("Vigenere") == 0;
    }

    public void bruteMode(){
        StringBuilder maxKey = new StringBuilder();
        StringBuilder currKey = new StringBuilder();
        int pos, counter = 0, currMatches, maxMatches = 0;
        char tempLetter;
        boolean carry;
        long starttime, endtime, totaltime;
        brute = true;

        List<String> wordBank = Arrays.asList("the","of","and","to","in","for","is","on","that","by","this","with",
                "you","it","not","or","be","are","from","at","as","your","all","have","new","more","an","was","we",
                "will","home","can","us","about","if","page","my","has","search","free","but","our","one","other",
                "do","no","information","time","they","site","he","up","may","what","which","their","news","out","use",
                "any","there","see","only","so","his","when","contact","here","business","who","web","also","now","help",
                "get","pm","view","online","first","am","been","would","how","were","me","services","some","these",
                "click","its","like","service","than","find","price","date","back","top","people","had","list","name",
                "just","over","state","year","day","into","email","two","health","world","re","next","used","go","work",
                "last","most","products","music","buy","data","make","them","should","product","system","post","her",
                "city","add","policy","number","such","please","available","copyright","support","message","after",
                "best","software","then","jan","good","video","well","where","info","rights","public","books","high",
                "school","through","each","links","she","review","years","order","very","privacy","book","items",
                "company","read","group","sex","need","many","user","said","de","does","set","under","general",
                "research","university","january","mail","full","map","reviews","program","life", "know", "games", "way",
                "days", "management", "part", "could", "great", "united", "hotel", "real", "item", "international",
                "center", "ebay", "must", "store", "travel", "comments", "made", "development", "report", "off",
                "member", "details", "line", "terms", "before", "hotels", "did", "send", "right", "type", "because",
                "local", "those", "using", "results", "office", "education", "national", "car", "design", "take",
                "posted", "internet", "address", "community", "within", "states", "area", "want", "phone", "dvd",
                "shipping", "reserved", "subject", "between", "forum", "family", "long", "based", "code", "show",
                "even", "black", "check", "special", "prices", "website", "index", "being", "women", "much", "sign",
                "file", "link", "open", "today", "technology", "south", "case", "project", "same", "pages", "uk",
                "version", "section", "own", "found", "sports", "house", "related", "security", "both", "county",
                "american", "photo", "game", "members", "power", "while", "care", "network", "down", "computer",
                "systems", "three", "total", "place", "end", "following", "download", "him", "without", "per", "access",
                "think", "north", "resources", "current", "posts", "big", "media", "law", "control", "water", "history",
                "pictures", "size", "art", "personal", "since", "including", "guide", "shop", "directory", "board",
                "location", "change", "white", "text", "small", "rating", "rate", "government", "children", "during",
                "usa", "return", "students", "shopping", "account", "times", "sites", "level", "digital", "profile",
                "previous", "form", "events", "love", "old", "john", "main", "call", "hours", "image", "department",
                "title", "description", "non", "insurance", "another", "why", "shall", "property", "class", "cd",
                "still", "money", "quality", "every", "listing", "content", "country", "private", "little", "visit",
                "save", "tools", "low", "reply", "customer", "december", "compare", "movies", "include", "college",
                "value", "article", "york", "man", "card", "jobs", "provide", "food", "source", "author", "different",
                "press", "learn", "sale", "around", "print", "course", "job", "canada", "process", "teen", "room", "stock");
        for(int i = 0; i < wordBank.size(); i++){
            wordBank.set(i, wordBank.get(i).toUpperCase());
        }
        for(int i = 0; i < key.length(); i++){
            maxKey.append("A");
        }
        key = maxKey.toString();
        maxKey.setLength(0);

        for(int i = 0; i < key.length(); i++){
            maxKey.append("Z");
        }
        starttime = System.nanoTime();
        while (key.compareTo(maxKey.toString()) != 0){
            currKey.setLength(0);
            carry = true;
            currMatches = 0;
            currKey.append(key);
            pos = key.length()-1;
            while (carry && pos >= 0){
                tempLetter = key.charAt(pos);
                tempLetter++;
                if (tempLetter > 90) {
                    carry = true;
                    tempLetter = 65;
                }else
                    carry=false;
                currKey.replace(pos,pos+1,String.valueOf(tempLetter));
                pos--;
            }
            decrypt();
            counter++;
            for(int i = 0; i < wordBank.size(); i++){
                if (dText.contains(wordBank.get(i))){
                    currMatches++;
                }
            }
            if(currMatches > maxMatches){
                System.out.println("Replaced key: " + key + " at " + maxMatches + " matches.");
                maxMatches = currMatches;
                maxKey.setLength(0);
                maxKey.append(key);
            }

            if (counter % 1000000 == 0){
                endtime = System.nanoTime();
                totaltime = endtime - starttime;
                System.out.println("Working on iteration: " + counter + " time " + totaltime/1000000000.0/60.0 + " minutes.");
                starttime = System.nanoTime();
            }
            key = currKey.toString();
            if (key.compareTo(maxKey.toString()) == 0){
                System.out.println("Most matches belongs to key: " + maxKey);
                System.out.println("Made it to the end");
                //stuff so we can set breakpoint here
            }
        }
        key = maxKey.toString();
        brute = false;
    }
 }
